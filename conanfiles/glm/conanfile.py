from conans import ConanFile, tools


class GlmConan(ConanFile):
    name = "glm"
    version = "<VERSION>"
    build_policy = "missing"
    url = "https://github.com/g-truc/glm"
    options = {"revision": "ANY"}
    default_options = "revision=<REV>"
    description = "Header only C++ mathematics library for graphics software."
    license = "MIT"

    def build(self):
        tools.get(
            "https://github.com/g-truc/glm/archive/"
            + str(self.options.revision)
            + ".zip"
        )

    def package(self):
        src_folder = "glm-" + str(self.options.revision) + "/glm"
        self.copy("*.hpp", src=src_folder, dst="glm")
        self.copy("*.inl", src=src_folder, dst="glm")
        self.copy("*.h", src=src_folder, dst="glm")

    def package_info(self):
        self.cpp_info.includedirs = [""]
