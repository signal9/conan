#include <celero/Celero.h>
#include <thread>
#include <chrono>


CELERO_MAIN

void demo() {
    std::this_thread::sleep_for(std::chrono::nanoseconds{1});
}

BASELINE(DemoSimple, Demo, 0, 0)
{
    demo();
}
