from conans import ConanFile, CMake, tools


class CeleroConan(ConanFile):
    name = "Celero"
    version = "<VERSION>"
    license = "Apache 2.0 License"
    author = "John Farrier"
    url = "https://github.com/DigitalInBlue/Celero"
    description = "C++ Benchmark Authoring Library/Framework "
    topics = ("benchmark", "microbenchmarks", "measurements")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": False}
    generators = "cmake"
    _revision = "<REV>"

    def source(self):
        tools.get(
            "https://github.com/DigitalInBlue/Celero/archive/"
            + str(self._revision)
            + ".zip"
        )

    def build(self):
        cmake = CMake(self)
        cmake.configure(
            source_dir="Celero-" + str(self._revision),
            defs={"CELERO_TREAT_WARNINGS_AS_ERRORS": "OFF"}
        )
        cmake.build()
        cmake.install()

    def package(self):
        self.copy("*.h", dst="include")
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["celero"]

