from conans import ConanFile, CMake, tools
from os.path import exists


class BustacheConan(ConanFile):
    name = "bustache"
    version = "<VERSION>"
    license = "Boost Software License"
    url = "https://github.com/jamboree/bustache"
    options = {"revision": "ANY", "shared": [True, False]}
    default_options = "revision=<REV>", "shared=True"
    settings = "os", "compiler", "build_type", "arch"
    build_policy = "missing"
    description = "C++14 implementation of {{ mustache }}, compliant with spec v1.1.3."

    def build(self):
        tools.get(
            "https://github.com/jamboree/bustache/archive/"
            + str(self.options.revision)
            + ".zip"
        )
        cmake = CMake(self)
        cmake.configure(source_dir="bustache-" + str(self.options.revision))
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["bustache"]
        self.cpp_info.libdirs = list(filter(exists, ("lib32", "lib64", "lib")))
