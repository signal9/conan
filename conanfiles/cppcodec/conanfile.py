from conans import ConanFile, tools


class CppcodecConan(ConanFile):
    name = "cppcodec"
    version = "<VERSION>"
    build_policy = "missing"
    url = "https://github.com/tplgy/cppcodec"
    options = {"revision": "ANY"}
    default_options = "revision=<REV>"
    description = "Header-only C++11 library to encode/decode base64, base64url, base32, base32hex and hex (a.k.a. base16) as specified in RFC 4648, plus Crockfords base32."
    license = "MIT"

    def build(self):
        tools.get(
            "https://github.com/tplgy/cppcodec/archive/"
            + str(self.options.revision)
            + ".zip"
        )

    def package(self):
        self.copy(
            "*.hpp",
            dst="cppcodec",
            src="cppcodec-" + str(self.options.revision) + "/cppcodec",
        )

    def package_info(self):
        self.cpp_info.includedirs = [""]
