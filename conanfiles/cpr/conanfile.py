from conans import ConanFile, CMake, tools
from os.path import exists


class CprConan(ConanFile):
    name = "cpr"
    version = "<VERSION>"
    license = "MIT"
    url = "https://github.com/whoshuu/cpr"
    options = {"revision": "ANY", "shared": [True, False]}
    default_options = "revision=<REV>", "shared=True"
    settings = "os", "compiler", "build_type", "arch"
    build_policy = "missing"
    description = "C++ Requests is a simple wrapper around libcurl inspired by the excellent Python Requests project."

    def build(self):
        tools.get(
            "https://github.com/whoshuu/cpr/archive/"
            + str(self.options.revision)
            + ".zip"
        )
        cmake = CMake(self)
        cmake.configure(
            defs={"BUILD_CPR_TESTS": "OFF", "USE_SYSTEM_CURL": "ON"},
            source_dir="cpr-" + str(self.options.revision),
        )
        cmake.build()

    def package(self):
        self.copy(
            "*.h", dst="include", src="cpr-" + str(self.options.revision) + "/include"
        )
        self.copy("*.so*", dst="lib", src="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["cpr"]
        self.cpp_info.libdirs = list(filter(exists, ("lib32", "lib64", "lib")))
