from conans import ConanFile, tools


class ClippConan(ConanFile):
    name = "clipp"
    version = "<VERSION>"
    build_policy = "missing"
    url = "https://github.com/muellan/clipp"
    options = {"revision": "ANY"}
    default_options = "revision=<REV>"
    description = "Easy to use, powerful & expressive command line argument parsing for modern C++ / single header / usage & doc generation"
    license = "MIT"

    def source(self):
        tools.download(
            "https://raw.githubusercontent.com/muellan/clipp/"
            + str(self.options.revision)
            + "/include/clipp.h",
            "clipp.h",
        )

    def package(self):
        self.copy("*.h")

    def package_info(self):
        self.cpp_info.includedirs = [""]
