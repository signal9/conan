from conans import ConanFile, tools


class Rangev3Conan(ConanFile):
    name = "range-v3"
    version = "<VERSION>"
    build_policy = "missing"
    url = "https://github.com/ericniebler/range-v3"
    options = {"revision": "ANY"}
    default_options = "revision=<REV>"
    description = "Range library for C++11/14/17. This code is the basis of a formal proposal to add range support to the C++ standard library."
    license = "Boost Software License"

    def build(self):
        tools.get(
            "https://github.com/ericniebler/range-v3/archive/"
            + str(self.options.revision)
            + ".zip"
        )

    def package(self):
        self.copy("*", src="range-v3-" + str(self.options.revision) + "/include")

    def package_info(self):
        self.cpp_info.includedirs = [""]
