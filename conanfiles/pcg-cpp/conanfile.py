from conans import ConanFile, tools


class PcgCppConan(ConanFile):
    name = "pcg-cpp"
    version = "<VERSION>"
    build_policy = "missing"
    url = "https://github.com/imneme/pcg-cpp"
    options = {"revision": "ANY"}
    default_options = "revision=<REV>"
    description = "This code provides an implementation of the PCG family of random number generators, which are fast, statistically excellent, and offer a number of useful features."
    license = "MIT"

    def build(self):
        tools.get(
            "https://github.com/imneme/pcg-cpp/archive/"
            + str(self.options.revision)
            + ".zip"
        )

    def package(self):
        self.copy("*.hpp", src="pcg-cpp-" + str(self.options.revision) + "/include")

    def package_info(self):
        self.cpp_info.includedirs = [""]
