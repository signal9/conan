from conans import ConanFile, tools, CMake
from os.path import exists

cmakefiles_txt = """\
project(uWebSockets LANGUAGES CXX)
cmake_minimum_required(VERSION 3.5)

if(NOT CMAKE_BUILD_TYPE)
	message(STATUS "Setting CMAKE_BUILD_TYPE to Release")
	set(CMAKE_BUILD_TYPE Release)
endif()

find_package(OpenSSL REQUIRED)
include_directories(SYSTEM ${OPENSSL_INCLUDE_DIR})

find_package(ZLIB REQUIRED)
include_directories(SYSTEM ${ZLIB_INCLUDE_DIRS})

add_definitions(-DUSE_ASIO)

set(CMAKE_CXX_STANDARD 11)

add_library(uWS SHARED Extensions.cpp Group.cpp WebSocketImpl.cpp Networking.cpp Hub.cpp Node.cpp WebSocket.cpp HTTPSocket.cpp Socket.cpp Epoll.cpp)
target_link_libraries(uWS PUBLIC ${ZLIB_LIBRARIES} ${OPENSSL_LIBRARIES})

install(TARGETS uWS DESTINATION lib)
install(FILES Asio.h Backend.h Epoll.h Extensions.h Group.h HTTPSocket.h Hub.h Libuv.h Networking.h Node.h Socket.h uWS.h WebSocket.h WebSocketProtocol.h DESTINATION include/uWS)
"""


class UWebSocketsConan(ConanFile):
    name = "uWebSockets"
    version = "<VERSION>"
    license = "Zlib"
    url = "https://github.com/uWebSockets/uWebSockets"
    options = {"revision": "ANY", "shared": [True, False]}
    default_options = "revision=<REV>", "shared=True"
    settings = "os", "compiler", "build_type", "arch"
    description = "Tiny WebSockets"
    build_policy = "missing"

    def build(self):
        tools.get(
            "https://github.com/uWebSockets/uWebSockets/archive/"
            + str(self.options.revision)
            + ".zip"
        )
        src_folder = "uWebSockets-" + str(self.options.revision)
        with open(src_folder + "/src/CMakeLists.txt", "w") as f:
            f.write(cmakefiles_txt)
        cmake = CMake(self)
        cmake.configure(source_dir=src_folder + "/src")
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["uWS"]
        self.cpp_info.defines = ["USE_ASIO"]
        self.cpp_info.libdirs = list(filter(exists, ("lib32", "lib64", "lib")))
