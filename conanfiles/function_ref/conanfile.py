import os

from conans import ConanFile, tools


class FunctionRefConan(ConanFile):
    name = "function_ref"
    version = "<VERSION>"
    license = "CC0 1.0 Universal"
    author = "Sy Brand"
    url = "https://github.com/TartanLlama/function_ref"
    description = "A lightweight, non-owning reference to a callable."
    no_copy_source = True
    _revision = "<REV>"

    def source(self):
        tools.download(
            "https://raw.githubusercontent.com/TartanLlama/function_ref/"
            + self._revision
            + "/include/tl/function_ref.hpp",
            "function_ref.hpp",
        )

    def package(self):
        self.copy("*.hpp", "include")
