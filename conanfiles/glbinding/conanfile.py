from conans import ConanFile, CMake, tools
from os.path import exists


class GlbindingConan(ConanFile):
    name = "glbinding"
    version = "<VERSION>"
    license = "MIT"
    url = "https://github.com/cginternals/glbinding"
    options = {"revision": "ANY", "shared": [True, False]}
    default_options = "revision=<REV>", "shared=True"
    settings = "os", "compiler", "build_type", "arch"
    build_policy = "missing"
    description = (
        "glbinding is an MIT licensed, cross-platform C++ binding for the OpenGL API."
    )

    def build(self):
        tools.get(
            "https://github.com/cginternals/glbinding/archive/"
            + str(self.options.revision)
            + ".zip"
        )
        cmake = CMake(self)
        cmake.configure(
            defs={
                "OPTION_BUILD_TESTS": "OFF",
                "OPTION_BUILD_GPU_TESTS": "OFF",
                "OPTION_BUILD_EXAMPLES": "OFF",
            },
            source_dir="glbinding-" + str(self.options.revision),
        )
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["glbinding"]
        self.cpp_info.libdirs = list(filter(exists, ("lib32", "lib64", "lib")))
