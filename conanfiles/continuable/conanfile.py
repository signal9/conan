from conans import ConanFile, tools


class ContinuableConan(ConanFile):
    name = "continuable"
    version = "<VERSION>"
    license = "MIT"
    url = "https://github.com/Naios/continuable"
    options = {"revision": "ANY"}
    default_options = "revision=<REV>"
    description = "Async C++14 platform independent continuation chainer providing light and allocation aware futures "
    build_policy = "missing"
    requires = "function2/[>=2.3]@signal9/stable"

    def source(self):
        tools.get(
            "https://github.com/Naios/continuable/archive/"
            + str(self.options.revision)
            + ".zip"
        )

    def package(self):
        self.copy("*", src="continuable-" + str(self.options.revision) + "/include")

    def package_info(self):
        self.cpp_info.includedirs = [""]
