from conans import ConanFile, tools


class DebugAssertConan(ConanFile):
    name = "debug_assert"
    version = "<VERSION>"
    build_policy = "missing"
    url = "https://github.com/foonathan/debug_assert"
    options = {"revision": "ANY"}
    default_options = "revision=<REV>"
    description = "Simple, flexible and modular assertion macro."
    license = "Zlib"

    def build(self):
        tools.get(
            "https://github.com/foonathan/debug_assert/archive/"
            + str(self.options.revision)
            + ".zip"
        )

    def package(self):
        self.copy("debug_assert.hpp", src="debug_assert-" + str(self.options.revision))

    def package_info(self):
        self.cpp_info.includedirs = [""]
