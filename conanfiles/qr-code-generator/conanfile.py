from conans import ConanFile, CMake, tools
from os.path import exists

cmakefiles_txt = """\
project(qrcodegenerator LANGUAGES CXX)
cmake_minimum_required(VERSION 3.5)

if(NOT CMAKE_BUILD_TYPE)
	message(STATUS "Setting CMAKE_BUILD_TYPE to Release")
	set(CMAKE_BUILD_TYPE Release)
endif()

set(CMAKE_CXX_STANDARD 14)

add_library(${PROJECT_NAME} SHARED BitBuffer.cpp QrCode.cpp QrSegment.cpp)

install(TARGETS ${PROJECT_NAME} DESTINATION lib)
install(FILES BitBuffer.hpp QrCode.hpp QrSegment.hpp DESTINATION include)
"""


class QrCodeGEneratorConan(ConanFile):
    name = "qr-code-generator"
    version = "<VERSION>"
    license = "MIT"
    url = "https://github.com/nayuki/QR-Code-generator"
    options = {"revision": "ANY", "shared": [True, False]}
    default_options = "revision=<REV>", "shared=True"
    settings = "os", "compiler", "build_type", "arch"
    build_policy = "missing"
    description = (
        "This project aims to provide the best and clearest QR Code generator library."
    )

    def build(self):
        tools.get(
            "https://github.com/nayuki/QR-Code-generator/archive/"
            + str(self.options.revision)
            + ".zip"
        )
        src_folder = "QR-Code-generator-" + str(self.options.revision)
        with open(src_folder + "/cpp/CMakeLists.txt", "w") as f:
            f.write(cmakefiles_txt)
        cmake = CMake(self)
        cmake.configure(source_dir=src_folder + "/cpp")
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["qrcodegenerator"]
        self.cpp_info.libdirs = list(filter(exists, ("lib32", "lib64", "lib")))
