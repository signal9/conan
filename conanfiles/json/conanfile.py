from conans import ConanFile, tools


class JsonConan(ConanFile):
    name = "json"
    version = "<VERSION>"
    build_policy = "missing"
    url = "https://github.com/nlohmann/json"
    options = {"revision": "ANY"}
    default_options = "revision=<REV>"
    description = "JSON for Modern C++ "
    license = "MIT"

    def source(self):
        tools.download(
            "https://raw.githubusercontent.com/nlohmann/json/develop/single_include/nlohmann/json.hpp",
            "json.hpp",
        )

    def package(self):
        self.copy("*.hpp")

    def package_info(self):
        self.cpp_info.includedirs = [""]
