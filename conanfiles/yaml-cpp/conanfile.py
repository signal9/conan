from conans import ConanFile, CMake, tools
from os.path import exists


class YamlCppConan(ConanFile):
    name = "yaml-cpp"
    version = "<VERSION>"
    license = "MIT"
    url = "https://github.com/jbeder/yaml-cpp"
    options = {"revision": "ANY", "shared": [True, False]}
    default_options = "revision=<REV>", "shared=True"
    settings = "os", "compiler", "build_type", "arch"
    build_policy = "missing"
    description = (
        "yaml-cpp is a YAML parser and emitter in C++ matching the YAML 1.2 spec."
    )

    def build(self):
        tools.get(
            "https://github.com/jbeder/yaml-cpp/archive/"
            + str(self.options.revision)
            + ".zip"
        )
        cmake = CMake(self)
        cmake.configure(
            defs={"YAML_CPP_BUILD_TOOLS": "OFF", "YAML_CPP_BUILD_CONTRIB": "OFF"},
            source_dir="yaml-cpp-" + str(self.options.revision),
        )
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["yaml-cpp"]
        self.cpp_info.libdirs = list(filter(exists, ("lib32", "lib64", "lib")))
