from conans import ConanFile, tools


class EigenConan(ConanFile):
    name = "eigen"
    version = "<VERSION>"
    build_policy = "missing"
    url = "https://bitbucket.org/eigen/eigen"
    options = {"revision": "ANY"}
    default_options = "revision=<REV>"
    description = "Eigen is a C++ template library for linear algebra: matrices, vectors, numerical solvers, and related algorithms."
    license = "BSD-3-Clause"

    def build(self):
        tools.get(
            "https://github.com/eigenteam/eigen-git-mirror/archive/"
            + str(self.options.revision)
            + ".zip"
        )

    def package(self):
        src_folder = "eigen-git-mirror-" + str(self.options.revision)
        self.copy("*", dst="Eigen", src=src_folder + "/Eigen")
        self.copy("*", dst="unsupported/Eigen", src=src_folder + "/unsupported/Eigen")

    def package_info(self):
        self.cpp_info.includedirs = [""]
