from conans import ConanFile, tools


class CxxprettyprintConan(ConanFile):
    name = "cxx-prettyprint"
    version = "<VERSION>"
    build_policy = "missing"
    url = "https://github.com/louisdx/cxx-prettyprint"
    options = {"revision": "ANY"}
    default_options = "revision=<REV>"
    description = "A pretty printing library for C++ containers."
    license = "Boost Software License"

    def source(self):
        tools.download(
            "https://raw.githubusercontent.com/louisdx/cxx-prettyprint/master/prettyprint.hpp",
            "prettyprint.hpp",
        )

    def package(self):
        self.copy("*.hpp")

    def package_info(self):
        self.cpp_info.includedirs = [""]
