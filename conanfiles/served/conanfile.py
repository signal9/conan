from conans import ConanFile, CMake, tools
from os.path import exists


class ServedConan(ConanFile):
    name = "served"
    version = "<VERSION>"
    license = "MIT"
    requires = "re2/latest@signal9/stable"
    url = "https://github.com/datasift/served"
    options = {"revision": "ANY"}
    default_options = "revision=<REV>"
    settings = "os", "compiler", "build_type", "arch"
    build_policy = "missing"
    description = "A C++11 RESTful web server library"
    generators = "cmake"

    def build(self):
        src_folder = "served-" + str(self.options.revision)
        tools.get(
            "https://github.com/datasift/served/archive/"
            + str(self.options.revision)
            + ".zip"
        )
        tools.replace_in_file(
            src_folder + "/CMakeLists.txt",
            "PROJECT (served)",
            """\
PROJECT (served)
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()
set(RE2_INCLUDE_DIR ${CONAN_INCLUDE_DIRS_RE2})
find_library(RE2_LIBRARY re2 PATHS ${CONAN_LIB_DIRS_RE2} NO_DEFAULT_PATH)
if($ENV{BOOST_ROOT})
	set(BOOST_ROOT $ENV{BOOST_ROOT})
endif()
""",
        )
        tools.replace_in_file(
            src_folder + "/CMakeLists.txt", "FIND_PACKAGE (RE2 REQUIRED)", ""
        )
        cmake = CMake(self)
        cmake.configure(
            defs={
                "CMAKE_INSTALL_PREFIX": self.package_folder,
                "BUILD_SHARED_LIBS": "ON",
                "SERVED_BUILD_TESTS": "OFF",
                "SERVED_BUILD_EXAMPLES": "OFF",
            },
            source_dir="served-" + str(self.options.revision),
        )
        cmake.build(target="install")

    def package_info(self):
        self.cpp_info.libs = ["served"]
        self.cpp_info.libdirs = list(filter(exists, ("lib32", "lib64", "lib")))
