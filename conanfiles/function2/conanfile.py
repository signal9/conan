from conans import ConanFile, tools


class Function2Conan(ConanFile):
    name = "function2"
    version = "<VERSION>"
    license = "Boost Software License"
    url = "https://github.com/Naios/function2"
    options = {"revision": "ANY"}
    default_options = "revision=<REV>"
    description = (
        "Improved drop-in replacement to std::function which supports move only types"
    )
    build_policy = "missing"

    def source(self):
        tools.download(
            "https://raw.githubusercontent.com/Naios/function2/master/include/function2/function2.hpp",
            "function2.hpp",
        )

    def package(self):
        self.copy("*.hpp")

    def package_info(self):
        self.cpp_info.includedirs = [""]
