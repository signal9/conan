from conans import ConanFile, tools


class CerealConan(ConanFile):
    name = "cereal"
    version = "<VERSION>"
    build_policy = "missing"
    url = "https://github.com/USCiLab/cereal"
    options = {"revision": "ANY"}
    default_options = "revision=<REV>"
    description = " A C++11 library for serialization "
    license = "BSD-3-Clause"

    def build(self):
        tools.get(
            "https://github.com/USCiLab/cereal/archive/"
            + str(self.options.revision)
            + ".zip"
        )

    def package(self):
        self.copy("*", src="cereal-" + str(self.options.revision) + "/include")

    def package_info(self):
        self.cpp_info.includedirs = [""]
