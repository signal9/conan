from conans import ConanFile, tools, Meson
from os import chdir
from pathlib import Path
from os.path import exists

MESON_BUILD = """\
project('url-cpp', 'cpp')
library('url', [{}],
include_directories: include_directories('include'),
install: true)
install_headers([{}], subdir: 'urlcpp')
"""


class UrlCppConan(ConanFile):
    name = "url-cpp"
    version = "<VERSION>"
    build_policy = "missing"
    url = "https://github.com/seomoz/url-cpp"
    options = {"revision": "ANY", "shared": [True, False]}
    default_options = "revision=<REV>", "shared=True"
    settings = "os", "compiler", "build_type", "arch"
    description = "C++ bindings for url parsing and sanitization "
    license = "MIT"

    def build(self):
        tools.get(
            "https://github.com/seomoz/url-cpp/archive/"
            + str(self.options.revision)
            + ".zip"
        )
        foldr = "url-cpp-" + str(self.options.revision)
        chdir(foldr)
        tools.save(
            "meson.build",
            MESON_BUILD.format(
                ",".join(map("'{}'".format, map(str, Path(".").glob("src/*.cpp")))),
                ",".join(map("'{}'".format, map(str, Path(".").glob("include/*.h")))),
            ),
        )
        meson = Meson(self)
        if self.options.shared:
            default_library = "shared"
        else:
            default_library = "static"
        meson.configure(
            source_folder=foldr,
            build_folder="build",
            args=[
                "-Dprefix=" + self.package_folder,
                "-Ddefault_library=" + default_library,
            ],
        )
        meson.build(targets=["install"])

    def package_info(self):
        self.cpp_info.libs = ["url"]
        self.cpp_info.libdirs = list(filter(exists, ("lib32", "lib64", "lib")))
