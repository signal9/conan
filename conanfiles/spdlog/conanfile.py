from conans import ConanFile, tools
from shutil import rmtree


class SpdlogConan(ConanFile):
    name = "spdlog"
    version = "<VERSION>"
    build_policy = "missing"
    url = "https://github.com/gabime/spdlog/tree/master/include/spdlog"
    options = {"revision": "ANY"}
    default_options = "revision=<REV>"
    description = "Very fast, header only, C++ logging library."
    license = "MIT"
    requires = "fmt/[>=5.1]@signal9/stable"

    def build(self):
        tools.get(
            "https://github.com/gabime/spdlog/archive/"
            + str(self.options.revision)
            + ".zip"
        )

    def package(self):
        src_folder = "spdlog-" + str(self.options.revision)
        rmtree(src_folder + "/include/spdlog/fmt/bundled")
        self.copy("*.h", src=src_folder + "/include")

    def package_info(self):
        self.cpp_info.includedirs = [""]
        self.cpp_info.defines = ["SPDLOG_FMT_EXTERNAL"]
