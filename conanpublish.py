from pathlib import Path
from os import environ
from sys import argv, exit
from tempfile import TemporaryDirectory as tmp_dir
from shutil import copytree
from os.path import join
from subprocess import run as run_s
import subprocess
import re
from github import Github
import semver

GITHUB_USER = environ["GITHUB_USER"]
GITHUB_TOKEN = environ["GITHUB_TOKEN"]
CONAN_USER = environ["CONAN_USER"]
CONAN_TOKEN = environ["CONAN_TOKEN"]

_repo = re.compile(r"github\.com/([^/]+)/([^/'\"]+)")
_vers = re.compile(r"\d+(\.\d+){0,2}(-[a-z0-9]+)?")
_name = re.compile(r"name.*=.*['\"](.*)['\"]")

_github = Github(GITHUB_USER, GITHUB_TOKEN)

CONAN_EXE = "conan"


def _run_tmp(f, d, *args, **kwargs):
    with tmp_dir() as tmp:
        dr = Path(join(tmp, d.name))
        copytree(d, dr)

        def _run(ar, *args, **kwargs):
            return run_s([CONAN_EXE] + ar, *args, cwd=dr, **kwargs)

        f(dr / "conanfile.py", _run, *args, **kwargs)


class _TagCmp:
    def __init__(self, obj):
        self.obj = obj

    def __lt__(self, other):
        return semver.compare_loose(self.obj[1], other.obj[1]) == -1

    def __gt__(self, other):
        return semver.compare_loose(self.obj[1], other.obj[1]) == 1

    def __eq__(self, other):
        return semver.compare_loose(self.obj[1], other.obj[1]) == 0

    def __le__(self, other):
        return semver.compare_loose(self.obj[1], other.obj[1]) <= 0

    def __ge__(self, other):
        return semver.compare_loose(self.obj[1], other.obj[1]) >= 0

    def __ne__(self, other):
        return semver.compare_loose(self.obj[1], other.obj[1]) != 0


def _last_tag(repo):
    tags = []
    for t in repo.get_tags():
        ver = _version(t.name)
        if not ver:
            continue
        tags.append((t, ver.format()))
    if not tags:
        return None
    tags.sort(key=_TagCmp, reverse=True)
    return tags[0][0]


def _conanfile_info(cf):
    text = cf.read_text()
    r = re.search(_repo, text)
    if not r:
        raise Exception("git repo not found")
    git_user, git_repo = r.group(1), r.group(2)
    repo = _github.get_repo(f"{git_user}/{git_repo}")
    last_tag = _last_tag(repo)
    last_commit = repo.get_commits()[0]
    return last_tag, last_commit


def _version(v):
    try:
        return semver.parse(v, loose=True)
    except:
        try:
            return semver.parse(re.search(_vers, v).group(0), loose=True)
        except:
            try:
                return semver.parse(re.search(_vers, v).group(0) + ".0", loose=True)
            except:
                return None


def _versions(t):
    if not t:
        return None, "1.0.9999"
    v = _version(t.name)
    base = f"{v.major}.{v.minor}"
    vers = f"{base}.{v.patch}"
    if v.prerelease:
        vers += "-" + "".join(v.prerelease)
    return vers, base + ".9999"


def _save_conan(cf, run, vers, sha):
    text = cf.read_text()
    text = re.sub("\t", "    ", text)
    text = re.sub("<VERSION>", vers, text)
    text = re.sub("<REV>", sha, text)
    cf.write_text(text)
    name = re.search(_name, text).group(1)
    run(
        ["info", "-r", "signal9", f"{name}/{vers}@signal9/stable"],
        timeout=10,
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL,
    )
    r = run(["export", ".", "signal9/stable"], stdout=subprocess.PIPE)
    if "not changed" in r.stdout.decode():
        print(f"{name} not changed, skipping")
        return
    run(
        [
            "upload",
            "-r",
            "signal9",
            f"{name}/{vers}@signal9/stable",
            "-c",
            "--retry",
            "5",
        ]
    )


def _conanfile(d):
    tag, commit = _conanfile_info(d / "conanfile.py")
    vers, git = _versions(tag)
    if vers:
        _run_tmp(_save_conan, d, vers, tag.commit.sha)
    else:
        _run_tmp(_save_conan, d, git, commit.sha)


def _setup_conan():
    run_s(
        [
            CONAN_EXE,
            "remote",
            "add",
            "signal9",
            "https://signal9.jfrog.io/artifactory/api/conan/conan",
        ],
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL,
    )
    run_s(
        [CONAN_EXE, "config", "set", "general.revisions_enabled=1"],
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL,
    )
    run_s(
        [CONAN_EXE, "user", "-p", CONAN_TOKEN, "-r", "signal9", CONAN_USER],
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL,
    )


if __name__ == "__main__":
    _setup_conan()
    cf = Path("conanfiles")
    if len(argv) == 2:
        _conanfile((cf / argv[1]).resolve())
        exit(0)
    for d in cf.iterdir():
        try:
            _conanfile(d.resolve())
        except Exception as e:
            print(f"Skipping {d}\n{e}")
